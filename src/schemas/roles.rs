use serde::{Deserialize, Serialize};

#[derive(Debug, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum Role {
    Owner,
    Admin,
    Moderator,
}

#[derive(Debug, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct InstanceRole {
    instance_id: i32,
    role: String,
}

#[derive(Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct UserRoles {
    pub user_id: i32,
    pub roles: Vec<InstanceRole>,
}
