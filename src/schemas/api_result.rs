use std::fmt::Display;
use std::{fmt, io::Cursor};

use fmt::Debug;
use rocket::{
    http::{ContentType, Status},
    response::{self, Responder},
    Response,
};
use serde::Serialize;
use serde_json::Value;

pub type ApiResult<T> = Result<T, ApiError>;

#[derive(Debug, Eq, PartialEq)]
pub struct ApiError {
    status: Status,
    reason: String,
    extra: Option<Value>,
}

impl fmt::Display for ApiError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        <ApiError as Debug>::fmt(&self, f)
    }
}

impl ApiError {
    pub fn builder() -> ApiErrorBuilder {
        ApiErrorBuilder::default()
    }

    pub fn internal_error() -> ApiError {
        ApiError::builder()
            .status(Status::InternalServerError)
            .build()
    }

    pub fn not_found() -> ApiError {
        ApiError::builder().status(Status::NotFound).build()
    }

    pub fn get_status(&self) -> Status {
        self.status
    }

    pub fn get_reason(&self) -> &str {
        &self.reason
    }
}

impl From<anyhow::Error> for ApiError {
    fn from(err: anyhow::Error) -> Self {
        log::error!("Internal server error: {:?}", err);

        let status = Status::InternalServerError;

        Self {
            status,
            reason: status.reason.into(),
            extra: None,
        }
    }
}

#[derive(Debug, Eq, PartialEq, Default)]
pub struct ApiErrorBuilder {
    status: Option<Status>,
    reason: Option<String>,
    extra: Option<Value>,
}

impl ApiErrorBuilder {
    pub fn status(mut self, status: Status) -> Self {
        self.status = Some(status);
        self
    }

    pub fn reason<D: Display>(mut self, reason: D) -> Self {
        self.reason = Some(reason.to_string());
        self
    }

    pub fn extra<S: Serialize>(mut self, extra: S) -> anyhow::Result<Self> {
        self.extra = Some(serde_json::to_value(extra)?);
        Ok(self)
    }

    pub fn build(self) -> ApiError {
        let status = self.status.unwrap_or(Status::InternalServerError);

        let reason = self.reason.unwrap_or_else(|| status.reason.into());

        ApiError {
            status,
            reason,
            extra: self.extra,
        }
    }
}

#[rocket::async_trait]
impl<'r> Responder<'r, 'static> for ApiError {
    fn respond_to(self, _: &'r rocket::Request<'_>) -> response::Result<'static> {
        #[derive(Debug, Serialize)]
        pub struct ErrorBody {
            status: u16,
            reason: String,
            #[serde(flatten, skip_serializing_if = "Option::is_none")]
            extra: Option<Value>,
        }

        impl From<&ApiError> for ErrorBody {
            fn from(err: &ApiError) -> Self {
                ErrorBody {
                    status: err.status.code,
                    reason: err.reason.clone(),
                    extra: err.extra.clone(),
                }
            }
        }

        let api_error_body = ErrorBody::from(&self);
        let api_error_body = serde_json::to_string(&api_error_body).unwrap();

        let response = Response::build()
            .status(self.status)
            .header(ContentType::JSON)
            .sized_body(api_error_body.len(), Cursor::new(api_error_body))
            .finalize();

        Ok(response)
    }
}
