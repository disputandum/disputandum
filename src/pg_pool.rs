use anyhow::Result;
use sqlx::postgres::PgPool;
use sqlx::postgres::PgPoolOptions;

use crate::getenv;
pub async fn pool_initialize(env_prefix: &str, application_name: &str) -> Result<PgPool> {
    let getenv = |env, default| getenv(env_prefix, env, default);

    let host = getenv("HOST", Some("127.0.0.1"))?;
    let port = getenv("PORT", Some("5432"))?;
    let dbname = getenv("NAME", None)?;
    let user = getenv("USER", None)?;
    let pass = getenv("PASS", None)?;

    let connection_url = format!(
        "postgresql://{user}:{pass}@{host}:{port}/{dbname}?application_name={application_name}",
        host = host,
        port = port,
        dbname = dbname,
        user = user,
        pass = pass,
        application_name = application_name,
    );

    let pg_pool = PgPoolOptions::new()
        .max_connections(5)
        .test_before_acquire(false)
        .connect(&connection_url)
        .await?;

    Ok(pg_pool)
}
