use anyhow::Result;
use redis::Client;
use redis::ConnectionAddr;
use redis::ConnectionInfo;

use crate::getenv;

pub fn initialize(env_prefix: &str) -> Result<Client> {
    let getenv = |env, default| getenv(env_prefix, env, default);

    let addr = getenv("REDIS_HOST", Some("127.0.0.1"))?;
    let port = getenv("REDIS_PORT", Some("6739"))?;
    let port = port.parse()?;

    let db = getenv("REDIS_DB", None)?;
    let db = db.parse()?;

    let username = getenv("REDIS_USER", None).ok();
    let passwd = getenv("REDIS_PASS", None).ok();

    let connection_info = ConnectionInfo {
        addr: Box::new(ConnectionAddr::Tcp(addr, port)),
        db,
        username,
        passwd,
    };

    let connection_manager = Client::open(connection_info)?;
    Ok(connection_manager)
}
