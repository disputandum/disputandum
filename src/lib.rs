pub mod schemas;

#[cfg(feature = "relational-db")]
pub mod pg_pool;
#[cfg(feature = "redis-db")]
pub mod redis;

pub fn getenv(env_prefix: &str, env: &str, default: Option<&str>) -> anyhow::Result<String> {
    use anyhow::anyhow;
    use std::env;

    let env_with_prefix = format!("{}_{}", env_prefix, env);
    let var_res = env::var(&env_with_prefix);

    match var_res {
        Ok(res) => Ok(res),
        Err(_) => {
            if let Some(default) = default {
                Ok(default.into())
            } else {
                Err(anyhow!("Missing env: {}", env_with_prefix))
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::getenv;
    use std::env;

    #[test]
    fn getenv_provided() {
        env::set_var("TEST_HOST", "127.0.0.121");

        let host = getenv("TEST", "HOST", "127.0.0.1".into()).unwrap();

        assert_eq!(host, "127.0.0.121");

        env::remove_var("TEST_HOST");
    }

    #[test]
    fn getenv_default() {
        let host = getenv("TEST", "HOST2", "127.0.0.1".into()).unwrap();

        assert_eq!(host, "127.0.0.1");
    }

    #[test]
    #[should_panic]
    fn getenv_not_found() {
        getenv("TEST", "HOST2", None).unwrap();
    }
}
